#include <stdio.h>
struct student {
    char Name[50];
    char subject [50];
    int count;
    float marks;
} s[10];

int main() {
    int i;
    printf("Enter informations of students:\n");

    
    for (i = 0; i < 5; ++i) {
        s[i].count = i + 1;
        printf("\nFor student number%d,\n", s[i].count);
        printf("Enter first name: ");
        scanf("%s", s[i].Name);
        printf("Enter subject name: ");
        scanf("%s", s[i].subject);
        printf("Enter marks: ");
        scanf("%f", &s[i].marks);
    }
    printf("\n\n Printing Information:\n\n");

    
    for (i = 0; i < 5; ++i) {
        printf("\nRoll number: %d\n", i + 1);
        printf("First name: ");
        puts(s[i].Name);
        printf("Subject Name: ");
        puts(s[i].subject);
        printf("Marks: %.1f", s[i].marks);
        printf("\n");
    }
    return 0;
}

